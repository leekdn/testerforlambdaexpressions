# testerForLambdaExpressions

This is a test repo to check whether expressions can be read(possibly written)

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.
```mermaid
graph TD
    b:::at
    f:::at
    j:::at
    n:::at
    p["λa[a:=w]"] --> b["@"]
    b --> c[w]
    b --> d

    e["λa[a:=w]"] --> f["@"] --> g[λw]
    --> a
    f --> h

    i["λa[a:=w]"] -->
    j["@"] --> k[λw]
    j --> l
    k --> w

    m["λa[a:=w]"] --> n["@"]
    n --> o[λa]
    n --> q
    o --> r[w]
    classDef at fill:none,stroke:#333,stroke-width:0px;
```
